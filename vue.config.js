const { green } = require('ansi-colors');
const { tick } = require('figures');
const { watch } = require('fs');
const { sync } = require('glob');
const { debounce } = require('lodash');
const minimatch = require('minimatch');
const { register } = require('ts-node');
const TransformPages = require('uni-read-pages');
const { webpack } = new TransformPages();
register({
  require: ['tsconfig-paths/register'],
  compilerOptions: { module: 'commonjs' }
});

const { devServer, define, mock } = getConfig();
const routes = getRoutes();

module.exports = {
  devServer,
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({ ...define, ROUTES: routes }),
    ],
  },
}

function getConfig() {
  const { proxy, ...config } = deepMerge(
    tryRequire('config/config'),
    tryRequire(`config/config.${process.env.UNI_ENV}`),
    tryRequire('config/config.local'),
  );
  if (proxy) {
    config.devServer = deepMerge({ proxy }, config.devServer);
  }
  if (config.mock !== false) {
    config.devServer = {
      ...config.devServer,
      before: app => mockerAPI(app),
    };
  }
  for (const key in config.define) {
    config.define[key] = JSON.stringify(config.define[key]);
  }
  return config;
}

function tryRequire(path) {
  const requireHandle = (path) => {
    const result = require(path);
    return result && result.default || result;
  };
  try { return requireHandle(`${path}.ts`) } catch (error) { }
  try { return requireHandle(`${path}.js`) } catch (error) { }
  try { return requireHandle(`${path}.json`) } catch (error) { }
  try { return requireHandle(path) } catch (error) { }
  return null;
}

function deepMerge(target, ...sources) {
  const isObject = value => {
    return value instanceof Object && !Array.isArray(value);
  };
  if (isObject(target)) {
    const mergeHandle = (target, source) => {
      for (const key in source) {
        if (isObject(source[key])) {
          if (!isObject(target[key])) {
            target[key] = {};
          }
          mergeHandle(target[key], source[key]);
        } else {
          target[key] = source[key];
        }
        if (target[key] === undefined) {
          delete target[key];
        }
      }
    };
    sources
      .filter(source => isObject(source))
      .forEach(source => {
        mergeHandle(target, source);
      });
  }
  return target;
}

function mockerAPI(app) {
  const mockFilePattern = 'mock/**/*.@(js|json|ts)';
  let handles;
  const includes = (path) => {
    const { exclude = [] } = mock || {};
    return !exclude.find(pattern => minimatch(path, pattern));
  };
  const updateHandles = debounce(() => {
    const currentHandles = {};
    let hasError = false;
    sync(mockFilePattern)
      .filter(path => includes(path))
      .forEach(path => {
        try {
          delete require.cache[require.resolve(path)];
          const subHandles = path.endsWith('.ts') ? require(path).default : require(path);
          for (const key in subHandles) {
            const [method, path] = key.split(/ +/);
            currentHandles[`${method.toUpperCase()} ${path}`] = typeof subHandles[key] !== 'function'
              ? (_, response) => { response.send(subHandles[key]) }
              : subHandles[key];
          }
        } catch (error) {
          hasError = true;
          console.error(error);
        }
      });
    if (handles && !hasError) {
      process.stdout.write(`${green(`${tick}  success`)}   Mock files parse success\n`);
    }
    handles = currentHandles;
  }, 100);
  updateHandles();
  watch('.', { recursive: true }, (_, filename) => {
    if (minimatch(filename, mockFilePattern) && includes(filename)) {
      updateHandles();
    }
  });
  app.all('*', (request, response, next) => {
    const handle = handles[`${request.method} ${request.path}`];
    if (handle) {
      handle(request, response, next);
    } else {
      next();
    }
  });
}

function getRoutes() {
  return webpack.DefinePlugin.runtimeValue(() => {
    const { routes } = new TransformPages({ includes: ['path', 'access'] });
    return JSON.stringify(routes);
  }, true);
}
