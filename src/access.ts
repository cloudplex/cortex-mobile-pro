import type { Models } from '@/cortex';

/**
 * 获取权限
 * @param initialState 初始化数据
 */
export default (
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  initialState: Models<'@@initialState'>['initialState'],
): Record<string, boolean> => {
  return {};
}
