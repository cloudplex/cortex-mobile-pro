import type { ILayoutConfig, IRequestConfig } from '@/cortex';

/** 获取初始化数据 */
export async function getInitialState() {
  return {};
}

/** 请求统一处理 */
export const request: IRequestConfig = {
}

/** 框架布局配置 */
export const layout: ILayoutConfig = () => {
  return {};
}
