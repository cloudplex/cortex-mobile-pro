declare module 'uview-ui' {
  import Vue from 'vue';
  export function install(vue: typeof Vue): void;
  export interface uForm extends Vue, uFormConstructor { }
  export interface uFormItem extends Vue, uFormItemConstructor {
    /** 父级表单 */
    uForm: uForm;
  }
  export interface uUpload extends Vue, uUploadConstructor { }
  export interface uVerificationCode extends Vue, uVerificationCodeConstructor { }
  export interface uCountDown extends Vue, uCountDownConstructor { }
  export interface uCountTo extends Vue, uCountToConstructor { }
  export interface uToast extends Vue, uToastConstructor { }
  export interface uTopTips extends Vue, uTopTipsConstructor { }
  export interface uCollapse extends Vue, uCollapseConstructor { }
  export interface uModal extends Vue, uModalConstructor { }
  export interface uWaterfall extends Vue, uWaterfallConstructor { }
  export interface uDropdown extends Vue, uDropdownConstructor { }
  export interface uTabsSwiper extends Vue, uTabsSwiperConstructor { }
  export interface uReadMore extends Vue, uReadMoreConstructor { }
  export {
    IFormRules,
    IFormRule,
    IUploadFiles,
    IUploadFile,
    IToastShowOptions,
    ITopTipsShowOptions
  };
}

declare module 'uview-ui/libs/function/md5' {
  /**
   * MD5加密
   * @param value 值
   * @see https://uviewui.com/js/md5.html
   */
  export function md5(value: string): string;
}

declare module 'uview-ui/libs/mixin/mpShare.js' {
  import Vue, { ComponentOptions } from 'vue';
  const mpShare: ComponentOptions<Vue>;
  export default mpShare;
}

declare global {
  declare module 'vue/types/vue' {
    interface Vue {
      $u: uView,
    }
  }
}

declare namespace UniApp {
  interface Uni {
    /**
     * @since 1.7.9
     * @see https://uviewui.com/components/quickstart.html#关于uni-u
     */
    $u: uView,
  }
}

interface uView {
  /**
   * 此方法为uniapp的uni.showToast的二次封装，方便用户使用，参数只能配置title和duration
   * @param title toast的消息内容
   * @param duration toast出现到消失的时间，单位ms，默认为 `1500`
   * @see https://uviewui.com/js/fastUse.html#toast-title-duration-1500
   */
  toast(title: string, duration?: number): void;
  /**
   * 此属性用于返回平台的名称，为小写的ios或android
   * @since 1.5.8
   * @see https://uviewui.com/js/fastUse.html#os
   */
  os(): string;
  /**
   * 此属性用于获取设备的信息，相当于uni.getSystemInfoSync()的效果
   * @since 1.5.8
   * @see https://uviewui.com/js/fastUse.html#sys
   */
  sys(): UniApp.GetSystemInfoResult;
  /** 
   * GET请求
   * @see https://uviewui.com/js/http.html#get-post-put-delete-url-params-header-then-res-catch-res
   */
  get: HTTPRequest;
  /**
   * POST请求
   * @see https://uviewui.com/js/http.html#get-post-put-delete-url-params-header-then-res-catch-res
   */
  post: HTTPRequest;
  /**
   * PUT请求
   * @see https://uviewui.com/js/http.html#get-post-put-delete-url-params-header-then-res-catch-res
   */
  put: HTTPRequest;
  /**
   * DELETE请求
   * @see https://uviewui.com/js/http.html#get-post-put-delete-url-params-header-then-res-catch-res
   */
  delete: HTTPRequest;
  /** HTTP请求处理 */
  http: HTTP;
  /** 
   * API集中管理
   * @see https://uviewui.com/js/apiManage.html
   */
  api?: API;
  /**
   * 节流
   * @param func 触发回调执行的函数
   * @param wait 时间间隔，单位ms，默认为 `500`
   * @param immediate 在开始还是结束处触发，默认为 `true` 开始处触发
   * @since 1.5.8
   * @see https://uviewui.com/js/debounce.html#节流
   */
  throttle<T extends Function>(func: T, wait?: number, immediate?: boolean): T;
  /**
   * 防抖
   * @param func 触发回调执行的函数
   * @param wait 时间间隔，单位ms，默认为 `500`
   * @param immediate 在开始还是结束处触发，默认为 `false` 结束处触发
   * @since 1.5.8
   * @see https://uviewui.com/js/debounce.html#防抖
   */
  debounce<T extends Function>(func: T, wait?: number, immediate?: boolean): T;
  /**
   * 对象深度克隆
   * @param object 需要被克隆的对象
   * @see https://uviewui.com/js/deepClone.html
   */
  deepClone<T extends Object>(object: T = {}): T;
  /**
   * 对象深度合并
   * @param target 目标对象
   * @param source 源对象
   * @see https://uviewui.com/js/deepMerge.html
   */
  deepMerge<T extends Object, U extends Object>(target: T = {}, source: U = {}): T & U;
  /**
   * 格式化时间
   * @param timestamp 任何合法的时间格式、秒或毫秒的时间戳
   * @param format 时间格式，默认为 `yyyy-mm-dd`
   * @see https://uviewui.com/js/time.html#格式化时间
   */
  timeFormat(timestamp: string | number, format?: string): string;
  /**
   * 格式化时间
   * @param timestamp 任何合法的时间格式、秒或毫秒的时间戳
   * @param format 时间格式，默认为 `yyyy-mm-dd`
   * @see https://uviewui.com/js/time.html#格式化时间
   */
  date(timestamp: string | number, format?: string): string;
  /**
   * 多久以前
   * @param time 时间戳
   * @param format 时间格式，默认为 `yyyy-mm-dd`，若为 `false`，则返回均为"多久之前"的结果
   * @see https://uviewui.com/js/time.html#多久以前
   */
  timeFrom(time: string | number, format?: string | false): string;
  /**
   * route路由跳转
   * @see https://uviewui.com/js/route.html
   */
  route(options: IRouteOptions): void;
  /**
   * route路由跳转
   * @param url 路由地址
   * @param params 传递的对象形式的参数，如 `{ name: 'lisa', age: 18 }`
   * @see https://uviewui.com/js/route.html
   */
  route(url: string, params?: BaseTypeRecord): void;
  /**
   * 数组乱序
   * @param array 一维数组
   * @see https://uviewui.com/js/randomArray.html
   */
  randomArray<T extends Array>(array: T): T;
  /**
   * 全局唯一标识符
   * @param length guid的长度，默认为 `32`，若为 `null`，则按 `rfc4122标准` 生成对应格式的随机数
   * @param firstU 首字母是否为"u"，默认为 `true`
   * @param radix 生成的基数，默认为 `62`
   * @see https://uviewui.com/js/guid.html
   */
  guid(length?: number | null, firstU?: boolean, radix?: number): string;
  /**
   * RGB转十六进制Hex
   * @param rgb RGB颜色值，如 `rgb(230, 231, 233)`
   * @see https://uviewui.com/js/colorSwitch.html#rgb转十六进制hex
   */
  rgbToHex(rgb: string): string;
  /**
   * 十六进制Hex转RGB
   * @param hex HEx颜色值，如 `#0afdce`
   * @see https://uviewui.com/js/colorSwitch.html#十六进制hex转rgb
   */
  hexToRgb(hex: string): string;
  /**
   * 颜色渐变
   * @param startColor 开始颜色值，可以是HEX或者RGB颜色值
   * @param endColor 结束颜色值，可以是HEX或者RGB颜色值
   * @param step 均分值，把开始值和结束值平均分成多少份
   * @see https://uviewui.com/js/colorSwitch.html#颜色渐变
   */
  colorGradient(startColor: string, endColor: string, step: number): string[];
  /**
   * 颜色透明度
   * @param color 颜色值，可以是HEX或者RGB颜色值
   * @param opacity 不透明度值，取值为0-1之间，默认为 `0.3`
   * @see https://uviewui.com/js/colorSwitch.html#颜色透明度
   */
  colorToRgba(color: string, opacity?: number): string;
  /** 
   * 预设颜色值
   * @see https://uviewui.com/js/color.html
   */
  color: Color;
  /**
   * 对象转URL参数
   * @param data 对象值，如 `{ name: 'lisa', age: 20 }`
   * @param isPrefix 是否在返回的字符串前加上"?"，默认为 `true`
   * @param arrayFormat 属性为数组的情况下的处理办法，默认为 `brackets`
   * @see https://uviewui.com/js/queryParams.html
   */
  queryParams(
    data: BaseTypeRecord,
    isPrefix?: boolean,
    arrayFormat?:
      | 'indices'
      | 'brackets'
      | 'repeat'
      | 'comma',
  ): string;
  /** 规则验证 */
  test: Test;
  /**
   * 随机数值
   * @param min 最小值，最小值可以等于该值
   * @param max 最大值，最大值可以等于该值
   * @see https://uviewui.com/js/random.html
   */
  random(min: number, max: number): number;
  /**
   * 随机数值
   * @param min 最小值，最小值可以等于该值
   * @param max 最大值，最大值可以等于该值
   * @see https://uviewui.com/js/random.html
   */
  trim(
    str: string,
    pos:
      | 'both'
      | 'left'
      | 'right'
      | 'all'
  ): string;
  /**
   * 获取节点
   * @param selector 选择器
   * @param all 是否返回全部节点信息，默认为 `false`
   * @see https://uviewui.com/js/getRect.html
   */
  getRect(selector: string, all?: boolean): Promise<UniApp.NodeInfo>;
  /**
   * 小程序分享
   * @see https://uviewui.com/js/mpShare.html
   */
  mpShare?: MpShare;
};

interface BaseTypeRecord {
  [key: string]: string | number | boolean;
}

type HTTPRequest = <T = any>(
  /** 地址 */
  url: string,
  /** 参数 */
  params?: BaseTypeRecord,
  /** 头部 */
  header?: BaseTypeRecord,
) => Promise<T>;

interface HTTP {
  /**
   * 配置参数
   * @param config 配置
   * @see https://uviewui.com/js/http.html#配置参数
   */
  setConfig(config?: IHTTPConfig): void;
  /** 拦截器 */
  interceptors: HTTPInterceptors;
};

interface IHTTPConfig {
  /** 请求的本域名 */
  baseUrl?: string;
  /** 请求方法 */
  method?:
  | 'GET'
  | 'POST'
  | 'PUT'
  | 'DELETE';
  /** 设置为json，返回后会对数据进行一次JSON.parse() */
  dataType?: 'json' | 'text';
  /** 是否显示请求中的loading */
  showLoading?: boolean;
  /** 请求loading中的文字提示 */
  loadingText?: string;
  /** 在此时间内，请求还没回来的话，就显示加载中动画，单位ms */
  loadingTime?: number;
  /** 是否在拦截器中返回服务端的原始数据 */
  originalData?: boolean;
  /** 展示loading的时候，是否给一个透明的蒙层，防止触摸穿透 */
  loadingMask?: boolean;
  /** 配置请求头信息 */
  header?: BaseTypeRecord;
}

interface HTTPInterceptors {
  /**
   * 请求拦截
   * @param config 配置
   * @see https://uviewui.com/js/http.html#何谓请求拦截
   */
  request(config: IHTTPInterceptorRequestConfig): IHTTPInterceptorRequestConfig;
  /**
   * 响应拦截
   * @param res 结果
   * @see https://uviewui.com/js/http.html#何谓响应拦截
   */
  response<T = any>(res: T): T | false;
};

interface IHTTPInterceptorRequestConfig {
  /** 请求地址 */
  url?: string;
  /** 请求头信息 */
  header?: BaseTypeRecord;
}

interface API {
  [name: string]: APIFunction;
}

type APIFunction = <T = any>(...args) => Promise<T>;

interface IRouteOptions {
  /** 类型，默认为 `navigateTo` */
  type?:
  | ('navigateTo' | 'to')
  | ('redirect' | 'redirectTo')
  | ('switchTab' | 'tab')
  | 'reLaunch'
  | ('navigateBack' | 'back');
  /** 路由地址，类型非 `navigateBack`/`back` 时为必填 */
  url?: string;
  /** 类型为 `navigateBack`/`back` 时用到，表示返回的页面数，默认为 `1` */
  delta?: number;
  /** 传递的对象形式的参数，如 `{ name: 'lisa', age: 18 }` */
  params?: BaseTypeRecord;
  /** 只在APP生效，默认为 `pop-in` */
  animationType?:
  | 'slide-in-right'
  | 'slide-in-left'
  | 'slide-in-top'
  | 'slide-in-bottom'
  | 'pop-in'
  | 'fade-in'
  | 'zoom-out'
  | 'zoom-fade-out'
  | 'none';
  /** 动画持续时间，单位ms, 默认为 `300` */
  animationDuration?: number;
}

interface Color {
  /** 主要颜色 */
  primary: string;
  /** 主要颜色(深) */
  primaryDark: string;
  /** 主要颜色(禁用) */
  primaryDisabled: string;
  /** 主要颜色(浅) */
  primaryLight: string;
  /** 错误颜色 */
  error: string;
  /** 错误颜色(深) */
  errorDark: string;
  /** 错误颜色(禁用) */
  errorDisabled: string;
  /** 错误颜色(浅) */
  errorLight: string;
  /** 警告颜色 */
  warning: string;
  /** 警告颜色(深) */
  warningDark: string;
  /** 警告颜色(禁用) */
  warningDisabled: string;
  /** 警告颜色(浅) */
  warningLight: string;
  /** 信息颜色 */
  info: string;
  /** 信息颜色(深) */
  infoDark: string;
  /** 信息颜色(禁用) */
  infoDisabled: string;
  /** 信息颜色(浅) */
  infoLight: string;
  /** 成功颜色 */
  success: string;
  /** 成功颜色(深) */
  successDark: string;
  /** 成功颜色(禁用) */
  successDisabled: string;
  /** 成功颜色(浅) */
  successLight: string;
  /** 主要文字颜色 */
  mainColor: string;
  /** 常规文字颜色 */
  contentColor: string;
  /** 次要文字颜色 */
  tipsColor: string;
  /** 占位文字颜色 */
  lightColor: string;
  /** 边框颜色 */
  borderColor: string;
  /** 背景颜色 */
  bgColor: string;
}

interface Test {
  /**
   * 是否验证码
   * @param value 值
   * @param len 验证码长度，默认为 `6`
   * @since 1.6.3
   * @see https://uviewui.com/js/test.html#是否验证码
   */
  code(value: any, len?: number): boolean;
  /**
   * 是否数组
   * @param value 值
   * @since 1.5.4
   * @see https://uviewui.com/js/test.html#是否数组
   */
  array(value: any): boolean;
  /**
   * 是否Json字符串
   * @param value 值
   * @since 1.5.4
   * @see https://uviewui.com/js/test.html#是否json字符串
   */
  jsonString(value: any): boolean;
  /**
   * 是否对象
   * @param value 值
   * @since 1.5.4
   * @see https://uviewui.com/js/test.html#是否对象
   */
  object(value: any): boolean;
  /**
   * 是否邮箱号
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否邮箱号
   */
  email(value: any): boolean;
  /**
   * 是否手机号
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否手机号
   */
  mobile(value: any): boolean;
  /**
   * 是否URL
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否url
   */
  url(value: any): boolean;
  /**
   * 是否为空
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否为空
   */
  isEmpty(value: any): boolean;
  /**
   * 是否普通日期
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否普通日期
   */
  date(value: any): boolean;
  /**
   * 是否十进制数值
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否十进制数值
   */
  number(value: any): boolean;
  /**
   * 是否整数
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否整数
   */
  digits(value: any): boolean;
  /**
   * 是否身份证号
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否身份证号
   */
  idCard(value: any): boolean;
  /**
   * 是否车牌号
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否车牌号
   */
  carNo(value: any): boolean;
  /**
   * 是否金额
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否金额
   */
  amount(value: any): boolean;
  /**
   * 是否汉字
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否汉字
   */
  chinese(value: any): boolean;
  /**
   * 是否字母
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否字母
   */
  letter(value: any): boolean;
  /**
   * 是否字母或者数字
   * @param value 值
   * @see https://uviewui.com/js/test.html#是否字母或者数字
   */
  enOrNum(value: any): boolean;
  /**
   * 是否包含某个值
   * @param str 字符串
   * @param subStr 子字符串
   * @see https://uviewui.com/js/test.html#是否包含某个值
   */
  contains(str: string, subStr: string): boolean;
  /**
   * 数值是否在某个范围内
   * @param number 数值
   * @param range 目标范围
   * @see https://uviewui.com/js/test.html#数值是否在某个范围内
   */
  range(number: number, range: [number, number]): boolean;
  /**
   * 字符串长度是否在某个范围内
   * @param str 字符串
   * @param range 目标范围
   * @see https://uviewui.com/js/test.html#字符串长度是否在某个范围内
   */
  rangeLength(str: string, range: [number, number]): boolean;
};

interface MpShare {
  /** 标题，默认为小程序名称，可自定义 */
  title: string;
  /** 路由路径，默认为当前页面路径，一般无需修改，QQ小程序不支持 */
  path: string;
  /** 分享图标地址，可以是本地文件路径、代码包文件路径或者网络图片路径。支持PNG及JPG，默认为当前页面的截图 */
  imageUrl: string;
};

interface uFormConstructor {
  /**
   * 设置验证规则
   * @param rules 验证规则
   * @see https://uviewui.com/components/form.html#验证规则属性
   */
  setRules(rules: IFormRules): void;
  /** 对整个表单进行重置，将所有字段值重置为初始值并移除验证结果 */
  resetFields(): void;
  /** 对整个表单进行验证 */
  validate(callback: (valid?: boolean) => any);
};

interface IFormRules {
  [name: string]: IFormRule[];
}

interface IFormRule {
  /** 触发验证的方式 */
  trigger?: SingleOrArray<'change' | 'blur'>;
  /** 值的类型 */
  type?:
  | 'string'
  | 'number'
  | 'boolean'
  | 'method'
  | 'regexp'
  | 'integer'
  | 'float'
  | 'array'
  | 'object'
  | 'enum'
  | 'date'
  | 'url'
  | 'hex'
  | 'email'
  | 'any';
  /** 是否必填 */
  required?: boolean;
  /** 正则匹配 */
  pattern?: RegExp;
  /** 最小值 / 最小长度 */
  min?: number;
  /** 最大值 / 最大长度 */
  max?: number;
  /** 值 / 长度 */
  len?: number;
  /** 值的枚举，匹配类型为 `enum` 的值 */
  enum?: string[];
  /** 通过空格填充的值 */
  whitespace?: boolean;
  /** 验证前对值进行转换，函数的参数为当前值，返回值为改变后的值 */
  transform?: (value: any) => any;
  /** 验证不通过时的提示信息 */
  message?: string;
  /** 自定义同步验证函数 */
  validator?: RuleValidator;
  /** 自定义异步验证函数 */
  asyncValidator?: RuleAsyncValidator;
};

type SingleOrArray<T> = T | T[];

type RuleValidator = (
  /** 当前验证的规则 */
  rule: IFormRule,
  /** 当前的值 */
  value: any,
  /** 验证完成时的回调 */
  callback: () => void,
) => boolean | void;

type RuleAsyncValidator = (
  /** 当前验证的规则 */
  rule: IFormRule,
  /** 当前的值 */
  value: any,
  /** 验证完成时的回调 */
  callback: (error?: Error) => void,
) => void;

interface uFormItemConstructor {
  /** 验证 */
  validation(): void;
  /** 验证状态 */
  validateState: 'validating' | 'success' | 'error' | undefined;
  /** 验证信息 */
  validateMessage: string | undefined;
}

interface uUploadConstructor {
  /** 内部文件列表 */
  lists: IUploadFiles;
  /** 上传状态 */
  uploading: boolean;
  /** 手动上传图片 */
  upload(): void;
  /** 清空内部文件列表 */
  clear(): void;
  /** 重新上传内部上传失败或者尚未上传的图片 */
  reUpload(): void;
  /**
   * 手动移除列表的某一个图片
   * @param index 索引
   */
  remove(index: number): void;
}

type IUploadFiles = IUploadFile[];

interface IUploadFile {
  /** 文件地址 */
  url: string;
  /** 上传错误状态 */
  error?: boolean;
  /** 上传进度，0-100之间的值 */
  progress?: number;
  /** 文件 */
  file?: File;
  /** 上传响应 */
  response?: any;
}

interface uVerificationCodeConstructor {
  /** 允许获取验证码 */
  canGetCode: boolean;
  /** 开始倒计时 */
  start(): void;
  /** 结束当前正在进行中的倒计时，设置组件为可以重新获取验证码的状态 */
  reset(): void;
}

interface uCountDownConstructor {
  /** 开始倒计时 */
  start(): void;
}

interface uCountToConstructor {
  /** 启动滚动 */
  start(): void;
  /** 继续滚动 */
  reStart(): void;
  /** 暂停滚动 */
  paused(): void;
}

interface uToastConstructor {
  /**
   * 显示
   * @param options 选项
   */
  show(options: IToastShowOptions): void;
}

interface IToastShowOptions {
  /** 标题 */
  title: string;
  /** 主题，默认为 `default` */
  type?:
  | 'default'
  | 'error'
  | 'success'
  | 'warning'
  | 'info'
  | 'primary';
  /** 显示图标，默认为 `true` */
  icon?: boolean;
}

interface uTopTipsConstructor {
  /**
   * 显示
   * @param options 选项
   */
  show(options: ITopTipsShowOptions): void;
}

interface ITopTipsShowOptions {
  /** 标题 */
  title: string;
  /** 主题，默认为 `primary` */
  type?: 'primary' | 'success' | 'info' | 'warning' | 'error';
  /** 显示的时间，单位ms */
  duration?: number;
}

interface uCollapseConstructor {
  /**
   * 重新初始化内部高度计算，用于异步获取内容的情形，请结合 `this.$nextTick()` 使用
   * @since 1.3.8
   */
  init(): void;
}

interface uModalConstructor {
  /** 仅清除加载状态 */
  clearLoading(): void;
}

interface uWaterfallConstructor {
  /** 清空列表数据 */
  clear(): void;
  /**
   * 移除数据
   * @param id 唯一值
   */
  remove(id: any): void;
}

interface uDropdownConstructor {
  /**
   * 设置高亮
   * @param index 索引，不填则清空内部的高亮
   */
  highlight(index?: number): void;
}

interface uTabsSwiperConstructor {
  /**
   * 设置x轴偏移量
   * @param dx x轴偏移量
   */
  setDx(dx: number): void;
  /**
   * 设置完成索引
   * @param current 当前索引
   */
  setFinishCurrent(current: number): void;
}

interface uReadMoreConstructor {
  /** 重新初始化组件内部高度计算过程，如果内嵌 `u-parse` 组件时可能需要用到 */
  init(): void;
}
