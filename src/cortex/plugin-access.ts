import { useModel } from './plugin-model';
import access from '@/access';

export function useAccess() {
  const { initialState } = useModel('@@initialState');
  return access(initialState);
}
