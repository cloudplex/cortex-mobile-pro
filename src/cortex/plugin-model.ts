import initialState from './plugin-initial-state';

export const models = {
  '@@initialState': initialState,
};

export type Model<T extends keyof typeof models> = {
  [key in keyof typeof models]: ReturnType<typeof models[T]>;
};
export type Models<T extends keyof typeof models> = Model<T>[T];

const modelValues: Record<string, any> = {};

export function useModel<T extends keyof Model<T>>(model: T): Model<T>[T];
export function useModel<T extends keyof Model<T>, U>(model: T, selector: (model: Model<T>[T]) => U): U;
export function useModel<T extends keyof Model<T>, U>(
  namespace: T,
  updater?: (model: Model<T>[T]) => U,
): typeof updater extends undefined ? Model<T>[T] : ReturnType<NonNullable<typeof updater>> {
  if (!modelValues[namespace]) {
    modelValues[namespace] = models[namespace]();
  }
  return updater?.(modelValues[namespace]) || modelValues[namespace];
};
