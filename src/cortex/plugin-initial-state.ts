import { getInitialState } from '@/app';
type ThenArg<T> = T extends Promise<infer U> ? U : T;

export default () => {
  const model = {
    initialState: undefined as ThenArg<ReturnType<typeof getInitialState>> | undefined,
    setInitialState(initialState: ThenArg<ReturnType<typeof getInitialState>>) {
      model.initialState = initialState;
    },
  };
  return model;
}
