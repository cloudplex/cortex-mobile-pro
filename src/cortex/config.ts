import { Models } from './plugin-model';
import { IDownloadFileOption, IRequestOptions, IUploadFileOption } from './plugin-request';

export function defineConfig(config: IDefineConfig) {
  return config;
}

export interface IDefineConfig {
  /** 定义全局常量 */
  define?: Record<string, any>;
  /**
   * 配置开发服务器
   * @see https://webpack.docschina.org/configuration/dev-server
   */
  devServer?: IDevServerConfig,
  /** 配置 mock */
  mock?: IMockConfig | false;
  /** 配置 openAPI */
  openAPI?: IOpenAPIConfig[];
  /**
   * 配置代理
   * @see https://github.com/chimurai/http-proxy-middleware#options
   */
  proxy?: Record<string, IProxyConfig>;
  [key: string]: any;
}

export interface IDevServerConfig {
  /** 头部 */
  headers?: Record<string, string>;
  /** 主机 */
  host?: string;
  /** 配置 https */
  https?: IHttpsConfig | boolean;
  /** 端口 */
  port?: number;
  [key: string]: any;
}

export interface IHttpsConfig {
  /** 服务器 RSA 私钥 */
  key?: string | Buffer;
  /** 服务器数字证书 */
  cert?: string | Buffer;
  /** CA证书 */
  ca?: string | Buffer;
}

export interface IProxyConfig {
  changeOrigin?: boolean;
  pathRewrite?: Record<string, string>;
  target?: string;
  ws?: boolean;
  [key: string]: any;
}

export interface IMockConfig {
  /** 排除文件 */
  exclude?: string[];
}

export interface IOpenAPIConfig {
  /** 项目名称 */
  projectName: string;
  /** 模型路径 */
  schemaPath: string;
}

export interface IRequestConfig {
  /** 错误处理 */
  errorHandler?: (error: {
    data: any;
    request: IRequestOptions | IUploadFileOption | IDownloadFileOption;
  }) => void;
  /** 请求拦截器 */
  requestInterceptors?: IRequestInterceptor[];
  /** 响应拦截器 */
  responseInterceptors?: IResponseInterceptor[];
}

export interface IRequestInterceptor {
  <T extends
    | IRequestOptions
    | IUploadFileOption
    | IDownloadFileOption
    >(options: T): Partial<T>
}

export interface IResponseInterceptor {
  <T extends
    | UniApp.RequestSuccessCallbackResult
    | UniApp.UploadFileSuccessCallbackResult
    | UniApp.DownloadSuccessData
    >(result: T): Partial<T>
}

export interface ILayoutConfig {
  (initData: Models<'@@initialState'>): {
    /** 页面改变时触发 */
    onPageChange?: (toRoute: Route, fromRoute?: Route) => void;
    /** 403页面路由 */
    exception403Route?: string;
    /** 404页面路由 */
    exception404Route?: string;
  };
}

export type Route = {
  /** 完整路径 */
  fullPath: string;
  /** 元标签 */
  meta: {
    /** 名称 */
    name: string;
    /** 页面路径 */
    pagePath: string;
    [key: string]: any;
  };
  /** 名称 */
  name: string;
  /** 参数 */
  params: Record<string, string>;
  /** 路径 */
  path: string;
  /** 查询 */
  query: Record<string, string>;
  [key: string]: any;
}
