import { request as requestConfig } from '@/app';

export interface IRequestOptions extends Omit<UniApp.RequestOptions, 'dataType' | 'success' | 'fail' | 'complete'> {
  /** 查询参数 */
  params?: Record<string, any>;
};

export interface IUploadFileOption extends Omit<UniApp.UploadFileOption, 'success' | 'fail' | 'complete'> {
  /** 查询参数 */
  params?: Record<string, any>;
  /** 监听上传进度变化 */
  onProgressUpdate?: (result: UniApp.OnProgressUpdateResult) => void;
};

export interface IDownloadFileOption extends Omit<UniApp.DownloadFileOption, 'success' | 'fail' | 'complete'> {
  /** 查询参数 */
  params?: Record<string, any>;
  /** 监听下载进度变化 */
  onProgressUpdate?: (result: UniApp.OnProgressDownloadResult) => void;
};

/**
 * 发起网络请求
 * 
 * 文档: http://uniapp.dcloud.io/api/request/request?id=request
 */
export async function request<T = any>(options: IRequestOptions) {
  return new Promise<T>((resolve, reject) => {
    try {
      uni.request({
        ...formatRequestOptions(options),
        dataType: 'text',
        success: result => {
          try {
            resolve(formatResultData(result));
          } catch (error) {
            reject(formatError(options, error));
          }
        },
        fail: error => {
          reject(formatError(options, error));
        }
      });
    } catch (error) {
      reject(formatError(options, error));
    }
  });
}

/**
 * 上传文件
 * 
 * 文档: http://uniapp.dcloud.io/api/request/network-file?id=uploadfile
 */
export async function uploadFile<T = any>(options: IUploadFileOption) {
  return new Promise<T>((resolve, reject) => {
    const { onProgressUpdate: onProgressUpdateHandle, ...baseOptions } = options;
    try {
      const uploadTask = uni.uploadFile({
        ...formatRequestOptions(baseOptions),
        success: result => {
          try {
            resolve(formatResultData(result));
          } catch (error) {
            reject(formatError(options, error));
          }
        },
        fail: error => {
          reject(formatError(options, error));
        }
      });
      if (onProgressUpdateHandle) {
        uploadTask.onProgressUpdate(onProgressUpdateHandle);
      }
    } catch (error) {
      reject(formatError(options, error));
    }
  });
}

/**
 * 下载文件
 * 
 * 文档: http://uniapp.dcloud.io/api/request/network-file?id=downloadfile
 */
export async function downloadFile(options: IDownloadFileOption) {
  return new Promise<string>((resolve, reject) => {
    const { onProgressUpdate: onProgressUpdateHandle, ...baseOptions } = options;
    try {
      const downloadTask = uni.downloadFile({
        ...formatRequestOptions(baseOptions),
        success: result => {
          try {
            resolve(formatResultData(result));
          } catch (error) {
            reject(formatError(options, error));
          }
        },
        fail: error => {
          reject(formatError(options, error));
        }
      });
      if (onProgressUpdateHandle) {
        downloadTask.onProgressUpdate(onProgressUpdateHandle);
      }
    } catch (error) {
      reject(formatError(options, error));
    }
  });
}

function formatRequestOptions<T extends
  | IRequestOptions
  | IUploadFileOption
  | IDownloadFileOption
>(options: T): T {
  const newOptions: any = { ...options };
  requestConfig.requestInterceptors?.forEach(requestInterceptor => {
    Object.assign(newOptions, requestInterceptor(newOptions));
  });
  if (newOptions.params) {
    newOptions.url += `${!/\?/.test(newOptions.url) ? '?' : '&'}${Object
      .keys(newOptions.params)
      .map(key => `${key}=${newOptions.params?.[key]}`)
      .join('&')}`;
    delete newOptions.params;
  }
  return newOptions;
}

function formatResultData(
  result:
    | UniApp.RequestSuccessCallbackResult
    | UniApp.UploadFileSuccessCallbackResult
    | UniApp.DownloadSuccessData,
) {
  requestConfig.responseInterceptors?.forEach(responseInterceptor => {
    Object.assign(result, responseInterceptor(result));
  });
  let data: any;
  if ((result as UniApp.DownloadSuccessData).tempFilePath) {
    if (/^[^23]/.test(result.statusCode.toString())) {
      throw Error(`请求错误 (${result.statusCode})`);
    }
    data = (result as UniApp.DownloadSuccessData).tempFilePath;
  } else {
    try {
      data = JSON.parse((result as UniApp.RequestSuccessCallbackResult).data as string);
    } catch (error) {
      data = (result as UniApp.RequestSuccessCallbackResult).data;
    }
    if (/^[^23]/.test(result.statusCode.toString())) {
      throw data;
    }
  }
  return data;
}

function formatError(request: IRequestOptions | IUploadFileOption | IDownloadFileOption, error: any) {
  try {
    requestConfig.errorHandler?.({ data: error, request });
  } catch (error) {
    return error;
  }
  return error;
}
