import Vue from 'vue';
import { Route } from './config';
import { useAccess } from './plugin-access';
import { useModel } from './plugin-model';
import { getInitialState, layout as layoutConfig } from '../app';

/** 初始化 */
export async function initialize() {
  const { setInitialState } = useModel('@@initialState');
  const initialState = await getInitialState();
  setInitialState(initialState);
}

/**
 * 守卫路由
 * @param app 应用实例
 */
export function guardRoutes(app: Vue) {
  app.$watch('$route', (toRoute: Route, fromRoute?: Route) => {
    const {
      onPageChange,
      exception403Route = '/pages/exception/403',
      exception404Route = '/pages/exception/404',
    } = layoutConfig(useModel('@@initialState'));
    const toRouteConfig = ROUTES.find(route => route.path === `/${toRoute.meta.pagePath}`);
    if (toRouteConfig) {
      const access = useAccess();
      if (access[toRouteConfig.access!] === false) {
        uni.redirectTo({ url: exception403Route });
      } else {
        onPageChange?.(toRoute, fromRoute);
      }
    } else if (toRoute.path !== '/preview-image') {
      uni.redirectTo({ url: exception404Route });
    }
  }, { immediate: true });
}
