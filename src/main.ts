import Vue from 'vue';
import App from '@/App.vue';
import { initialize, guardRoutes } from '@/cortex';
import uView from 'uview-ui';

initialize().then(() => {
  Vue.use(uView);
  Vue.config.productionTip = false;
  const app = new App;
  guardRoutes(app);
  app.$mount();
});
